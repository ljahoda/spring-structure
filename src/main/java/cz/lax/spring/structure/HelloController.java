package cz.lax.spring.structure;

import cz.lax.spring.structure.common.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.*;

/**
 * The type Hello controller.
 */
@Controller
@RequestMapping("/")
public class HelloController extends AbstractGuiController {

	/**
	 * Print welcome.
	 *
	 * @param model the model
	 * @return the model and view
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello world!");
		return new ModelAndView("hello", model);
	}
}