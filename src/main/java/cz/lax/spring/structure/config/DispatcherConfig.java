package cz.lax.spring.structure.config;

import org.springframework.context.annotation.*;
import org.springframework.web.servlet.*;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.freemarker.*;

import java.util.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 10.03.14
 * Email: lukas@lukasjahoda.cz
 */
@Configuration
@ComponentScan("cz.lax.spring.structure")
@EnableWebMvc
public class DispatcherConfig extends WebMvcConfigurerAdapter {

	// freemarker
	private static final String FM_RESOLVER_SUFFIX = ".ftl";
	private static final String FM_RESOLVER_CONTENT_TYPE = "text/html; charset=utf-8";
	private static final String FM_LOADER_PATH = "WEB-INF/pages/";
	private static final String FM_AUTO_IMPORT_PROP = "auto_import";
	private static final String FM_AUTO_IMPORT_VALUE = "spring.ftl as spring, common/master.ftl as master";
	private static final String FM_OUTPUT_ENCODING_PROP = "output_encoding";
	private static final String FM_OUTPUT_ENCODING_VALUE = "UTF-8";
	private static final String FM_DEFAULT_ENCODING_PROP = "default_encoding";
	private static final String FM_DEFAULT_ENCODING_VALUE = "UTF-8";

	// resource handlers
	private static final String RESOURCE_HANDLER_PATTERN_CSS = "/css/**";
	private static final String RESOURCE_HANDLER_LOCATION_CSS = "/css/";
	private static final String RESOURCE_HANDLER_PATTERN_JS = "/js/**";
	private static final String RESOURCE_HANDLER_LOCATION_JS = "/js/";

	/**
	 * Sets view resolver.
	 *
	 * @return the view resolver
	 */
	@Bean
	public ViewResolver setupViewResolver() {
		FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
		resolver.setSuffix(FM_RESOLVER_SUFFIX);
		resolver.setContentType(FM_RESOLVER_CONTENT_TYPE);
		return resolver;
	}

	/**
	 * Gets freemarker config.
	 *
	 * @return the freemarker config
	 */
	@Bean
	public FreeMarkerConfigurer getFreemarkerConfig() {
		FreeMarkerConfigurer result = new FreeMarkerConfigurer();
		result.setTemplateLoaderPath(FM_LOADER_PATH);
		Properties properties = new Properties();
		properties.setProperty(FM_AUTO_IMPORT_PROP, FM_AUTO_IMPORT_VALUE);
		properties.setProperty(FM_OUTPUT_ENCODING_PROP, FM_OUTPUT_ENCODING_VALUE);
		properties.setProperty(FM_DEFAULT_ENCODING_PROP, FM_DEFAULT_ENCODING_VALUE);
		result.setFreemarkerSettings(properties);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(RESOURCE_HANDLER_PATTERN_CSS).addResourceLocations(RESOURCE_HANDLER_LOCATION_CSS);
		registry.addResourceHandler(RESOURCE_HANDLER_PATTERN_JS).addResourceLocations(RESOURCE_HANDLER_LOCATION_JS);
	}
}
