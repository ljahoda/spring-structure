package cz.lax.spring.structure.config;

import org.springframework.context.annotation.*;
import org.springframework.context.support.*;
import org.springframework.web.servlet.i18n.*;

import java.util.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 10.03.14
 * Email: lukas@lukasjahoda.cz
 */
@Configuration
public class AppConfig {

	private static final String DEFAULT_LANGUAGE = "cs";

	// messages
	private static final String MESSAGES_PATH = "i18n/messages";
	private static final String MESSAGES_ENCODING = "UTF-8";

	/**
	 * Locale resolver.
	 *
	 * @return the session locale resolver
	 */
	@Bean
	public SessionLocaleResolver localeResolver() {
		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(new Locale(DEFAULT_LANGUAGE));
		return localeResolver;
	}

	/**
	 * Message source.
	 *
	 * @return the resource bundle message source
	 */
	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasename(MESSAGES_PATH);
		source.setDefaultEncoding(MESSAGES_ENCODING);
		source.setUseCodeAsDefaultMessage(true);
		return source;
	}
}
