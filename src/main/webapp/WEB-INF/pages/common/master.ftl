<#macro main>
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <#include 'head.ftl' />
    </head>
    <body>
        <#nested />
        <#include 'include.ftl' />
    </body>
    </html>
</#macro>